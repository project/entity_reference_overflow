CONTENTS OF THIS FILE
------------------------

* Introduction
* Requirements
* Installation
* Configuration

INTRODUCTION
---------------

This module inserts entities into the output of an Entity Reference field, in
addition to the values that are set in the field. This can be helpful with
creating "Related Content" sections that need to allow for both manual selection
as well as a dynamic fallback.

This module uses the configuration of the Entity Reference field to determine
where to pull additional content from.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/entity_reference_overflow

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/entity_reference_overflow

REQUIREMENTS
---------------

No special requirements

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
----------------

* Node field widget setup
  * Create a reference field on a node that references any other node types already
  defined on the site.
  * In the "Manage Display" settings of the node you are configuring, use the
  "Reference Overflow" widget.
  * Settings for the widget include:
    * View Mode: Select the view mode that you would like the referenced nodes to
    be displayed as.
    * Minimum Items: Select the amount of referenced nodes that you would like to
    be displayed.
    * Fields: Select the fields that have already been defined on the node you are
    configuring to relate to. Be sure that this field or fields are already defined on the
    node types that are being referenced. This setting will be used to dynamically grab related
    nodes that have the same fields and values as those defined here.
* Paragraph field widget setup
  * Use the same settings as the Node setup, however, simply on a Paragraph entity.

MAINTAINERS
--------------

Current maintainers:
* James Nettik (jnettik) - https://www.drupal.org/user/741478