<?php

namespace Drupal\entity_reference_overflow\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\field\FieldConfigInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field formatter for dynamic Entity Reference fields.
 *
 * If there are extra spaces for the field formatter to use, add new rows of
 * similarly tagged nodes.
 *
 * @FieldFormatter(
 *   id = "entity_reference_overflow",
 *   label = @Translation("Reference Overflow"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceOverflowFormatter extends EntityReferenceEntityFormatter {

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $fieldManager;

  /**
   * Constructs an EntityReferenceOverflowFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $field_manager
   *   The entity field manager service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    LoggerChannelFactoryInterface $logger_factory,
    EntityTypeManagerInterface $entity_type_manager,
    EntityDisplayRepositoryInterface $entity_display_repository,
    EntityFieldManagerInterface $field_manager
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $logger_factory, $entity_type_manager, $entity_display_repository);
    $this->fieldManager = $field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    // @todo add config for factoring in other tagging fields.
    return [
      'min_items' => 3,
      'ref_fields' => [],
      'sort_field' => 'created',
    ] + parent::defaultSettings();
  }

  /**
   * Calculates the remaining number of items needed to hit the threshold.
   *
   * @param int $item_count
   *   The number of items to compare against.
   *
   * @return int
   *   The number of remaining items needed.
   */
  protected function calcItemsNeeded(int $item_count) {
    return $this->getSetting('min_items') - $item_count;
  }

  /**
   * Get the entity type key to filter IDs by.
   *
   * @param string $entity_type
   *   The entity type the reference field is targeting.
   */
  protected function getEntityIdKey($entity_type) {
    return $this->entityTypeManager
      ->getStorage($entity_type)
      ->getEntityType()
      ->getKey('id');
  }

  /**
   * Get the IDs of already selected items to exclude from our query.
   *
   * @param \Drupal\Core\Entity\EntityInterface[] $items
   *   The array if items.
   *
   * @return array
   *   The array of IDs.
   */
  protected function getExclusionIds(array $items) {
    $ids = [];
    foreach ($items as $item) {
      $ids[] = $item->id();
    }
    return $ids;
  }

  /**
   * Get a query looking for the related items.
   *
   * @param string $target_entity_type
   *   The target entity type to query for.
   * @param \Drupal\Core\Entity\ContentEntityInterface $host_entity
   *   The entity our field is attached to.
   * @param int $items_needed
   *   The number of items we need to return.
   * @param array $bundles
   *   The target bundles to pull from.
   * @param array $exclude_ids
   *   Misc entity IDs to exclude from the query.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The entity query of related items.
   */
  protected function relatedItemQuery(
    string $target_entity_type,
    ContentEntityInterface $host_entity,
    int $items_needed,
    array $bundles = [],
    array $exclude_ids = []
  ) {
    $id_key = $this->getEntityIdKey($target_entity_type);
    $query = $this->entityTypeManager->getStorage($target_entity_type)->getQuery();
    $query
      ->condition('type', $bundles, 'IN')
      ->condition('status', 1)
      ->sort($this->getSetting('sort_field'), 'DESC')
      ->range(0, $items_needed)
      ->accessCheck(FALSE);

    // If we have items to exclude, exclude them.
    if (!empty($exclude_ids)) {
      $query->condition($id_key, $exclude_ids, 'NOT IN');
    }

    // If we're targeting the same entity type as the host, exclude it's entity.
    if ($target_entity_type === $host_entity->getEntityTypeId()) {
      $query->condition($id_key, $host_entity->id(), '<>');
    }

    // Filter by configured related fields.
    foreach ($this->getSetting('ref_fields') as $field_name) {
      $field = $host_entity->get($field_name);
      if (!$field->isEmpty()) {
        $ids = array_map(function ($item) {
          return $item['target_id'];
        }, $field->getValue());
        $query->condition($field_name, $ids, 'IN');
      }
    }

    return $query;
  }

  /**
   * Gets the render arrays of the related entities.
   *
   * @param \Drupal\Core\Entity\EntityInterface[] $entities
   *   The entities to be returned as render arrays.
   *
   * @return array
   *   The render arrays.
   */
  protected function getRelatedRenders(array $entities) {
    $renders = [];
    foreach ($entities as $entity) {
      $renders[] = $this->entityTypeManager
        ->getViewBuilder($entity->getEntityTypeId())
        ->view($entity, $this->getSetting('view_mode'));
    }
    return $renders;
  }

  /**
   * Get the reference fields for use in our query.
   *
   * @param string $entity_type
   *   The entity type to get fields for.
   * @param string $bundle
   *   The bundle to get fields for.
   *
   * @return array
   *   The list of reference fields keyed by machine name.
   */
  protected function getReferenceFields($entity_type, $bundle) {
    $reference_fields = [];
    $entity_fields = $this->fieldManager->getFieldDefinitions($entity_type, $bundle);

    foreach ($entity_fields as $field) {
      $is_referencable = (
        // Filter out base fields.
        $field instanceof FieldConfigInterface &&
        // Only get entity references.
        $field->getType() == 'entity_reference' &&
        // Filter out the current field.
        $field->getName() != $this->fieldDefinition->getName()
      );

      if ($is_referencable) {
        $reference_fields[$field->getName()] = $field->getLabel();
      }
    }

    return $reference_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $ref_fields = $this->getReferenceFields($form['#entity_type'], $form['#bundle']);

    $elements['min_items'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Minimum Items'),
      '#description' => $this->t('The number of items that should always be displayed.'),
      '#default_value' => $this->getSetting('min_items'),
    ];

    $elements['ref_fields'] = [
      '#type' => 'select',
      '#title' => $this->t('Fields'),
      '#description' => $this->t('Fields to relate content by.'),
      '#default_value' => $this->getSetting('ref_fields'),
      '#options' => $ref_fields,
      '#multiple' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $ref_fields = $this->getSetting('ref_fields');
    $summary[] = $this->t('Minimum items: @min_items', ['@min_items' => $this->getSetting('min_items')]);

    if (!empty($ref_fields)) {
      $summary[] = $this->t('Related by: @ref_fields', [
        '@ref_fields' => implode(', ', $ref_fields),
      ]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
    $element = parent::viewElements($items, $langcode);
    $items_needed = $this->calcItemsNeeded($items->count());

    // If we don't need any more items, return what we have.
    if ($items_needed <= 0) {
      return $element;
    }

    // Get field settings to help sort through the query.
    $entity_type = $this->getFieldSetting('target_type');
    $bundles = $this->getFieldSetting('handler_settings')['target_bundles'] ?? [];
    $exclude_ids = $this->getExclusionIds($items->referencedEntities());
    $related_ids = $this->relatedItemQuery(
      $entity_type,
      $items->getEntity(),
      $items_needed,
      $bundles,
      $exclude_ids
    )->execute();

    // Create render arrays of our entities.
    $related_items = $this->entityTypeManager
      ->getStorage($entity_type)
      ->loadMultiple($related_ids);
    $related_renders = $this->getRelatedRenders($related_items);
    return array_merge($element, $related_renders);
  }

}
